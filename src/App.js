//import logo from './logo.svg';
import './App.css';
import React from 'react';

import { Boton } from './Boton';
import { Buscador } from './Buscador';
import { Contador } from './Contador';
import { Item } from './Item';
import { Lista } from './Lista';
import Container from '@mui/material/Container';

/* const tareasDefault = [
  { text: 'Jugar con rocco', completed: true },
  { text: 'Hacer tarea video juegos', completed: false },
  { text: 'Comprar pan', completed: false },
  { text: 'Completar issues', completed: false },
   { text: 'crear repositorio', completed: false },
]; */
function App() {
  //------Persistencia de datos---------
  const localStorageTareas = localStorage.getItem('TAREAS_V1');
  let parsedTareas;

  if (!localStorageTareas) {
    localStorage.setItem('TAREAS_V1', JSON.stringify([]));
  } else {
    parsedTareas = JSON.parse(localStorageTareas);
  }

  //-------guardar tareas en el estado-------
  const guardarTareas = (newTarea) => {
    const stringTareas = JSON.stringify(newTarea);
    localStorage.setItem('TAREAS_V1', stringTareas);
    setTareas(newTarea);
  };

  // C O N T A D O R
  //Estado
  const [tareas, setTareas] = React.useState(parsedTareas);
  // Variable de tareas completadoas
  const completedTareas = tareas.filter((tarea) => !!tarea.completed).length;
  // Variable que almacena el total de tareas
  const totalTareas = tareas.length;

  //Crear la función --> Completar tarea
  // eslint-disable-next-line no-unused-vars
  const completarTarea = (text) => {
    // eslint-disable-next-line eqeqeq
    const tareaIndex = tareas.findIndex((tarea) => tarea.text == text);
    const nuevasTareas = [...tareas];
    nuevasTareas[tareaIndex].completed = true;
    guardarTareas(nuevasTareas);
  };

  // BUSCADOR

  //Crear el estado
  const [buscandoValor, setBuscandoValor] = React.useState('');

  //Crear un array vacio
  let buscarTareas = [];

  //Condicional
  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tarea) => {
      const tareaText = tarea.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }

  //Crear la función --> Eliminar tarea
  // eslint-disable-next-line no-unused-vars
  const eliminarTarea = (text) => {
    // eslint-disable-next-line eqeqeq
    const tareaIndex = tareas.findIndex((tarea) => tarea.text == text);
    const nuevasTareas = [...tareas];
    nuevasTareas.splice(tareaIndex, 1);
    guardarTareas(nuevasTareas);
  };

  return (
    <Container
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100vw',
        height: '100vh',
      }}
    >
      <div
        style={{
          backgroundColor: '#8477C8',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
          padding: '30px',
          borderRadius: '10px',
        }}
      >
        <Contador total={totalTareas} completed={completedTareas} />
        <Buscador
          buscandoValor={buscandoValor}
          setBuscandoValor={setBuscandoValor}
        />
        <Lista>
          {buscarTareas.map((tarea) => {
            //console.log(tarea.text);
            return (
              <Item
                text={tarea.text}
                completed={tarea.completed}
                //completarTarea={completarTarea}
                onComplete={() => completarTarea(tarea.text)}
                borrar={() => eliminarTarea(tarea.text)}
                key={tarea.text}
              />
            );
          })}
        </Lista>
        <Boton />
      </div>
    </Container>
  );
}

export default App;
