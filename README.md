# Tareas app
Esta app tiene como finalidad guardar y gestionar tus tareas a realizar

# Install
```npm
npm install
```

# Usage
```bash
npm start
```

# Contributing
Proyecto creado para aprender en la Gestión y Planeación de Software.

# License
tareasapp is released under the [MIT License](https://opensource.org/licenses/MIT).